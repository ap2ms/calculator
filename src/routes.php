<?php
/**
 * Created by PhpStorm.
 * User: Matar
 * Date: 05.04.2019
 * Time: 21:30
 */
use Symfony\Component\Routing;
$routes = new Routing\RouteCollection();
$routes->add('base', new Routing\Route('/', ['base' => null,'_controller'=>'App\Controllers\LeapYearController::index'], array(), array(), '', array(), array('GET','HEAD')));
$routes->add('calculate', new Routing\Route('calculate', [ 'calculate' => null,'_controller' => 'App\Controllers\LeapYearController::calculate'], array(), array(), '', array(), array('POST')));
return $routes;