<?php
namespace App\Controllers;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Calculator\Standart;
use App\Services\Calculator\Polish;
class LeapYearController
{
    /**
     * Возвращает вьюху с версткой калькулятора.
     * @return object
     */
    public function index()
    {
        ob_start();
        require_once __DIR__.'/../../src/pages/view.html';
        return new Response(ob_get_clean());
    }
    /**
     * Обращается в соответствующий класс для вычисления выражения по выбранному методу(обычный/польская запись).
     * @return string
     */
    //создать слой абстракции над классами Standart и Polish что
    // бы избавиться от условного оператора и дергать один сервис
    public function calculate(Request $request){
        $mode=$request->request->get('mode');
        $expression=$request->request->get('expression');
        if ($mode=='Polish'){
            $result=new Polish($expression);
        }else{
            $result=new Standart($expression);
        }
        $result=$result->getResult();
        //$result=$mode;
        return new Response($result);
    }
}