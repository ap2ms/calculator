<?php
/**
 * Created by PhpStorm.
 * User: Matar
 * Date: 06.04.2019
 * Time: 19:45
 */

namespace App\Services\Calculator;


abstract class Calculate
{
    public $expression;
    public $result;
    public function __construct($expression)
    {
        $this->expression=$expression;
    }
    /**
     * Выисляет уже приведенную строку по алгоритму вычисления выражений для польской записи
     * и присваевает св-ву result результат вычислений.
     * @return void
     */
    public function calculate($str){
        $stack=[];
        $last_key_in_stack=0;
        $unspaces_str = preg_replace('/\s+/', '', $str);
        $str_to_array=explode(',',$unspaces_str);
        $operands = ['+', '*', '/', '^', '-'];
        foreach ($str_to_array as $key => $val){
            if (!in_array($val, $operands)) {//если текущий val - цифра помещаю в стек
                $last_key_in_stack++;
                $stack[$last_key_in_stack]=$val;
            }else{
                $cur=$stack[$last_key_in_stack-0];
                $prev=$stack[$last_key_in_stack-1];
                $last_element=($val=='^')?pow($prev,$cur):eval('return '."$prev".$val."$cur".';');//для возведения в степень приходится ис-ть pow так как символ ^ в данном контексте бесполезен
                unset($stack[$last_key_in_stack-0]);
                unset($stack[$last_key_in_stack-1]);
                $stack[$last_key_in_stack-1]="$last_element";
                $last_key_in_stack=$last_key_in_stack-1;

            }
        }
        $this->result=$stack[1];
    }
    abstract public function getResult();
}