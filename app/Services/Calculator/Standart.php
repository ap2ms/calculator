<?php
/**
 * Created by PhpStorm.
 * User: Matar
 * Date: 06.04.2019
 * Time: 19:52
 */

namespace App\Services\Calculator;

class Standart extends Calculate
{
    public $result;
    public $expression;
    public function __construct($expression)
    {
        parent::__construct($expression);
        $this->toPolish($this->expression);
        $this->clearFormat();
        $this->calculate($this->result);
    }
    /**
     * Преобразует 'стандартную' запись выражения в обратную польскую запись
     * и присваевает св-ву result результат вычислений.
     * @return void
     */
    public function toPolish($expression){
        $unspaces_str = preg_replace('/\s+/', '', $expression);
        $operands = ['+', '*', '/', '^', '-','(',')'];
        $exit_str='';
        $stack=[];
        $str_to_array=str_split($unspaces_str);
        $last_key_in_stack=0;
        foreach ($str_to_array as $key=>$val){
            if(!in_array($val, $operands)){
                $exit_str=$exit_str."$val".',';
            }else{
                if(empty($stack) || $val=='('){
                    $stack[$last_key_in_stack]=$val;
                    $last_key_in_stack++;
                }else{
                    if($this->get_priority($val)>$this->get_priority($stack[$last_key_in_stack-1])){
                        $stack[$last_key_in_stack]=$val;
                        $last_key_in_stack++;
                    }else{
                        if($val==')'){
                            while ($stack[$last_key_in_stack-1] != '(') {
                                $exit_str=$exit_str.$stack[$last_key_in_stack-1].',';
                                unset($stack[$last_key_in_stack-1]);
                                $last_key_in_stack--;
                            }
                            unset($stack[$last_key_in_stack-1]);
                            $last_key_in_stack--;
                        }else {
                            $exit_str = $exit_str.$stack[$last_key_in_stack - 1];
                            unset($stack[$last_key_in_stack - 1]);
                            $stack[$last_key_in_stack - 1] = $val;
                            $last_key_in_stack++;
                        }
                    }
                }
            }
        }
        while(!empty($stack)){
            $exit_str=$exit_str.$stack[max(array_keys($stack))].',';
            unset($stack[max(array_keys($stack))]);
        }
        $this->result=$exit_str;
    }
    /**
     * Возвращает приоритет для переданного в метод оператора.
     * @return integer
     */
    private function get_priority($operand){
        switch ($operand) {
            case '(': return 0;
            case ')': return 0;
            case '+': return 2;
            case '-': return 2;
            case '*': return 3;
            case '/': return 3;
            case '^': return 4;
            default: return 5;
        }
    }
    /**
     * Данный метод вызывается только после преобразования 'стандартного' выражения в польскую запись
     * добавляет разделители в данном случае это запятые и присваевает получившуюся строку св-ву result.
     * @return void
     */
    public function clearFormat(){
        $str_to_array=explode(',',$this->result);
        $formatted_str='';
        foreach ($str_to_array as $key=>$val){
            if(strlen($val)>1){
                $part_str=implode(',', str_split($val));
                $formatted_str=$formatted_str.$part_str.',';

            }else{
                $formatted_str=$formatted_str.$val.',';
            }
        }
        $this->result=rtrim($formatted_str,", ");
    }
    /**
     * Возвращает результат вычислений.
     * @return string
     */
    public function getResult(){
        return $this->result;
    }
}