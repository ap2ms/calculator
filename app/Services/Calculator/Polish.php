<?php
/**
 * Created by PhpStorm.
 * User: Matar
 * Date: 06.04.2019
 * Time: 19:53
 */

namespace App\Services\Calculator;

class Polish extends Calculate
{
    public $result;
    public $expression;
    public function __construct($expression)
    {
        parent::__construct($expression);
        $this->calculate($this->expression);
    }
    /**
     * Возвращает результат вычисления.
     * @return string
    */
    public function getResult(){
        return $this->result;
    }

}