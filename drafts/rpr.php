<?php
if(!empty($_POST['expression'])){
    if($_POST['mode']=='standart'){
        to_polish($_POST['expression']);
    }else{
        calculate($_POST['expression']);
    }
}
function calculate($str){
    $stack=[];
    $last_key_in_stack=0;
    $unspaces_str = preg_replace('/\s+/', '', $str);
    $str_to_array=explode(',',$unspaces_str);
    $operands = ['+', '*', '/', '^', '-'];
    foreach ($str_to_array as $key => $val){
        if (!in_array($val, $operands)) {//если текущий val - цифра помещаю в стек
            $last_key_in_stack++;
            $stack[$last_key_in_stack]=$val;
        }else{
            $cur=$stack[$last_key_in_stack-0];
            $prev=$stack[$last_key_in_stack-1];
            $last_element=($val=='^')?pow($prev,$cur):eval('return '."$prev".$val."$cur".';');//для возведения в степень приходится ис-ть pow так как символ ^ в данном контексте бесполезен
            unset($stack[$last_key_in_stack-0]);
            unset($stack[$last_key_in_stack-1]);
            $stack[$last_key_in_stack-1]="$last_element";
            $last_key_in_stack=$last_key_in_stack-1;

        }
    }
    echo $stack[1];
}
//'3+4*2/(1-5)^2';//342*15−2^/+   |342*1−52^/+            //'(6+9-5)/(8+1*2)+7';//
function to_polish($expression){
    $unspaces_str = preg_replace('/\s+/', '', $expression);
    $operands = ['+', '*', '/', '^', '-','(',')'];
    $exit_str='';
    $stack=[];
    $str_to_array=str_split($unspaces_str);
    $last_key_in_stack=0;
    foreach ($str_to_array as $key=>$val){
        if(!in_array($val, $operands)){
            $exit_str=$exit_str."$val".',';
        }else{
            if(empty($stack) || $val=='('){
                $stack[$last_key_in_stack]=$val;
                $last_key_in_stack++;
            }else{
                if(get_priority($val)>get_priority($stack[$last_key_in_stack-1])){
                    $stack[$last_key_in_stack]=$val;
                    $last_key_in_stack++;
                }else{
                    if($val==')'){
                        while ($stack[$last_key_in_stack-1] != '(') {
                            $exit_str=$exit_str.$stack[$last_key_in_stack-1].',';
                            unset($stack[$last_key_in_stack-1]);
                            $last_key_in_stack--;
                        }
                        unset($stack[$last_key_in_stack-1]);
                        $last_key_in_stack--;
                    }else {
                        $exit_str = $exit_str.$stack[$last_key_in_stack - 1];
                        unset($stack[$last_key_in_stack - 1]);
                        $stack[$last_key_in_stack - 1] = $val;
                        $last_key_in_stack++;
                    }
                }
            }
        }
    }
    while(!empty($stack)){
        $exit_str=$exit_str.$stack[max(array_keys($stack))].',';
        unset($stack[max(array_keys($stack))]);
    }
    clear_format($exit_str);
}
function clear_format($polish_string){
    $str_to_array=explode(',',$polish_string);
    $formatted_str='';
    foreach ($str_to_array as $key=>$val){
        if(strlen($val)>1){
            $part_str=implode(',', str_split($val));
            $formatted_str=$formatted_str.$part_str.',';

        }else{
            $formatted_str=$formatted_str.$val.',';
        }
    }
    calculate(rtrim($formatted_str,", "));
}
function get_priority($operand){
    switch ($operand) {
        case '(': return 0;
        case ')': return 0;
        case '+': return 2;
        case '-': return 2;
        case '*': return 3;
        case '/': return 3;
        case '^': return 4;
        default: return 5;
    }
}