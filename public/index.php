<?php
/**
 * Created by PhpStorm.
 * User: msaubanov
 * Date: 01.04.2019
 * Time: 14:34
 */
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$routes = include __DIR__.'/../src/routes.php';
$container = include __DIR__.'/../app/container.php';
$request = Request::createFromGlobals();
$response = $container->get('loader')->handle($request);
$response->send();

